package uk.co.actualcode.vadalmqreceiver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@Slf4j
public class VadalMqReceiverApplication {

	public static final String VADALQ = "vadalQ";

	public static void main(String[] args) {
		SpringApplication.run(VadalMqReceiverApplication.class, args);
	}

	@Autowired
	public MsgRepo msgRepo;

	@Bean
	public Queue vadalQueue() {
		return new Queue(VADALQ, false);
	}

	@RabbitListener(queues = VADALQ)
	public void listen(String in) {
		log.info("Message read from vadalQ : " + in);
		msgRepo.save(new Msg(in));
	}

	@GetMapping("/put/{p}")
	public String put(@PathVariable String p) {
		Msg save = msgRepo.save(new Msg(p));
		return save.toString();
	}

	@GetMapping("/get")
	public Iterable<Msg> get() {
		Iterable<Msg> all = msgRepo.findAll();
		msgRepo.deleteAll();
		return all;
	}

}
