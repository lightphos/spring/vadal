package uk.co.actualcode.vadalmqreceiver;

import org.springframework.data.repository.CrudRepository;

public interface MsgRepo extends CrudRepository<Msg, Long> {
}
