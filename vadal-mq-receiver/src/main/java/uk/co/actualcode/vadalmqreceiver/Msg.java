package uk.co.actualcode.vadalmqreceiver;

import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Msg {

    @Id
    @GeneratedValue
    private Long id;
    private String m;

    public Msg(String m) {
        this.m = m;
    }
}