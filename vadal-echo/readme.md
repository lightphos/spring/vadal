# Kong JWT and Kubernetes

## Set up
Using deskop docker in mac

```
Engine: 24.0.7

Compose: v2.23.3-desktop.2

Credential Helper: v0.7.0

Kubernetes: v1.28.2
```

### MacOS install
Install brew and use brew to install java 11, maven, helm.

### Kong
Install kong helm:

delete any previous PVC:

```

kubectl get pvc                         
NAME                     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
data-kong-postgresql-0   Bound    pvc-abe89a90-d639-46ae-b93e-264359e1304f   8Gi        RWO            hostpath       19h

kubectl delete pvc --all
```

Using helm to install kong.

```

helm install kong kong/kong --set admin.enabled=true --set admin.http.enabled=true --set postgresql.enabled=true --set postgresql.postgresqlUsername=kong --set postgresql.postgresqlDatabase=kong --set env.database=postgres


NAME: kong
LAST DEPLOYED: Tue Jan  2 17:32:09 2024
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
To connect to Kong, please execute the following commands:

HOST=$(kubectl get svc --namespace default kong-kong-proxy -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
PORT=$(kubectl get svc --namespace default kong-kong-proxy -o jsonpath='{.spec.ports[0].port}')
export PROXY_IP=${HOST}:${PORT}
curl $PROXY_IP

Once installed, please follow along the getting started guide to start using
Kong: https://docs.konghq.com/kubernetes-ingress-controller/latest/guides/getting-started/
```


Check installation

```
kubectl get svc

NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                         AGE
kong-kong-admin                NodePort       10.98.112.102    <none>        8001:32687/TCP,8444:31762/TCP   49s
kong-kong-manager              NodePort       10.107.191.219   <none>        8002:32432/TCP,8445:32528/TCP   49s
kong-kong-proxy                LoadBalancer   10.109.163.180   localhost     80:31757/TCP,443:30641/TCP      49s
kong-kong-validation-webhook   ClusterIP      10.101.108.14    <none>        443/TCP                         49s
kong-postgresql                ClusterIP      10.103.27.103    <none>        5432/TCP                        49s
kong-postgresql-hl             ClusterIP      None             <none>        5432/TCP                        49s

```

## Build vadal-echo and deploy it

Make sure JAVA_HOME is set then:

`mvn spring-boot:build-image`

```
docker images | grep echo 

vadal-echo         0.0.1-SNAPSHOT      e36ffa9bc47f   44 years ago    264MB

```
Remove any previous deployment

`kubectl delete deployment vadal-echo`

`kubectl create deployment vadal-echo --image=vadal-echo:0.0.1-SNAPSHOT`

```
kubectl get deploy
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
kong-kong    1/1     1            1           8m17s
vadal-echo   1/1     1            1           22s

kubectl get svc kong-kong-admin
NAME              TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)                         AGE
kong-kong-admin   NodePort   10.98.112.102   <none>        8001:32687/TCP,8444:31762/TCP   25h

```

Create service for vadal-echo using the following:

```
apiVersion: v1
kind: Service
metadata:
  labels:
    app: vadal-echo
  name: vadal-echo
spec:
  ports:
    - port: 80
      protocol: TCP
      targetPort: 8080
  selector:
    app: vadal-echo
```

`kubectl apply -f ../infra/kong/vadal-echo-svc.yml`

```
kubectl get svc | grep echo
vadal-echo                     ClusterIP      10.110.239.118   <none>        80/TCP                          19h
```
We will using kong to access this service

Add Konga UI:

`kubectl apply -f ../infra/kong/konga.yml`

```
kubectl get svc | grep konga
konga-svc                      NodePort       10.102.254.224   <none>        1337:31299/TCP                  52s
```

Browse konga

`http://localhost:31299`

Connect the konga ui to kong admin NodePort using 
your local machine host ip and kongs admin port
in this case:

http://192.168.0.161:32687

## JWT access restriction

Using HS256.

https://jwt.io

Will need token for the payload: (from jwt.io)

Create the ingress to vadal-echo (../infra/kong/jwt.yml)

`kubectl apply -f ../infra/kong/jwt.yml`

Check the ingress:

```
kubectl describe ingress

Rules:
  Host        Path  Backends
  ----        ----  --------
  localhost   
              /echo   vadal-echo:80 (10.1.0.24:8080)
Annotations:  konghq.com/plugins: app-jwt
              konghq.com/strip-path: true
Events:       <none>
```

Check the plugin is jwt within Kong:

```
curl -i -X GET http://localhost:32687/plugins    
HTTP/1.1 200 OK
Date: Tue, 02 Jan 2024 20:12:08 GMT
Content-Type: application/json; charset=utf-8
Connection: keep-alive
Access-Control-Allow-Origin: *
Access-Control-Allow-Credentials: true
Content-Length: 725
X-Kong-Admin-Latency: 11
Server: kong/3.5.0

{"next":null,"data":[{"updated_at":1704225110,"consumer":null,"service":null,"instance_name":null,"enabled":true,"config":{"secret_is_base64":false,"cookie_names":[],"header_names":["authorization"],"key_claim_name":"iss","anonymous":null,"claims_to_verify":null,"maximum_expiration":0,"run_on_preflight":true,"uri_param_names":["jwt"]},"route":{"id":"a942ad61-0322-5a07-93cf-9bc7ad49fa78"},"tags":["k8s-name:app-jwt","k8s-namespace:default","k8s-kind:KongPlugin","k8s-uid:f2cf5ec9-ce5d-4ac7-9cea-e66ee5eea111","k8s-group:configuration.konghq.com","k8s-version:v1","managed-by-ingress-controller"],"id":"bea0bb5c-92ae-4d24-aa1b-db4763861db9","protocols":["grpc","grpcs","http","https"],"name":"jwt","created_at":1704225110}]}
```

https://jwt.io/#debugger-io

export JWT=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6InZhZGFsIiwiaXNzIjoidmFkYWwtaXNzdWVyIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.ic8C6zvg53uElRdZetSMwG7JceXMjx3eaCkJ3-HqJ8aCWvigWL_B1rY-bRlPPHVUyFhstPBtzcezBok-xeEotXOHttd1R4WfskEyS7tYBIsgSt9Tfsf4rkcq9TA1hfLnE3-NEGwwUVsA4exgx54Jaqvr48AE7HeMi2Pf74C3T9bws7PvepDyrYR83cgLehhxZcdiZthMwg6WJeoe5BsnKygUA-pIhrOLVpKMFBw0TnAYWLOeyWSNua-dzyRudP4jilH8ViimLIJEuf8NH2VPdJwXnparrQ99JioaasFHFy8oZzhZoaro_u72iS1VNOkKTcby0BCgNS-xNu1wLCrpfA

curl -H "Authorization:Bearer ${JWT}" localhost/echo
{"message":"No credentials found for given 'iss'"}%                                                 

NOTE:

    The most recent kong kubernetes is unable to find the secret from k8s.
    Something missing in jwt.yml or a bug in kong/k8s interaction.

Another way is to set up the jwt plugin in kong itself using the kong api.

## Kong API consumer and JWT access

First find the management port:


```
kubectl get svc | grep kong-admin
kong-kong-admin                NodePort       10.98.112.102    <none>        8001:32687/TCP,8444:31762/TCP   23h
```

In this case 32687

Set up consumer in kong:

`curl -d "username=vadal-jwt-k8s&custom_id=vadal-id" http://localhost:32687/consumers/`

`curl -i -X GET http://localhost:32687/consumers`

The vadal-echo service should be present:

`curl -i http://localhost:32687/services`

Set up the jwt for the consumer:


`curl -X POST http://localhost:32687/consumers/vadal-jwt-k8s/jwt -H "Content-Type: application/x-www-form-urlencoded"`

NOTE To delete

    curl -X DELETE http://localhost:32687/consumers/vadal-jwt-k8s/jwt/94d32b8c-b732-4e56-aba1-42aa2d6e0664

```
curl -i http://localhost:32687/consumers/vadal-jwt-k8s/jwt
{"next":null,"data":[{"rsa_public_key":null,"algorithm":"HS256","key":"rLAs27wyyzlVLhYj04hPDSZyVo0DKh6E","tags":null,"id":"89709e07-e60e-40ae-a3b6-e53276a4eb9f","consumer":{"id":"b379cae3-b312-4e44-bfc1-5bc5109216cd"},"secret":"mpvYYoCntZXAlYuM854GyAd7jzLkykEY","created_at":1704301348}]}
```

### JWT Token

Use the details above to get the jwt -> jwt.io

set in the jwt.io browser site, algorithm HS256:

iss -> rLAs27wyyzlVLhYj04hPDSZyVo0DKh6E << the key

secret -> mpvYYoCntZXAlYuM854GyAd7jzLkykEY << in the verify signature part
secret is base64 encoded

The jwt from the jwt.io site is (you can use this to see the details in jwt.io):


`export JWT=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6InZhZGFsIiwiaXNzIjoickxBczI3d3l5emxWTGhZajA0aFBEU1p5Vm8wREtoNkUiLCJhZG1pbiI6dHJ1ZSwiaWF0IjoxNTE2MjM5MDIyfQ.4-yT__GJURDRb7iL1oe9dGHDRCj58FVkHOX6yw_8gbE`

call vadal-echo with the header.

```
curl -i -H "Authorization: Bearer ${JWT}" localhost/echo

HTTP/1.1 200 
Content-Type: application/json
Transfer-Encoding: chunked
Connection: keep-alive
Date: Wed, 03 Jan 2024 17:18:15 GMT
X-Kong-Upstream-Latency: 481
X-Kong-Proxy-Latency: 12
Via: kong/3.5.0
X-Kong-Request-Id: f4d4d06a5985e014dcdb652ab8195b01

{"timestamp":"2024-01-03T17:18:15.820113","headers":{"host":"localhost","connection":"keep-alive","x-credential-identifier":"rLAs27wyyzlVLhYj04hPDSZyVo0DKh6E","x-forwarded-proto":"http","x-forwarded-host":"localhost","x-forwarded-port":"80","x-forwarded-path":"/echo","x-forwarded-prefix":"/echo","x-real-ip":"192.168.65.3","x-kong-request-id":"f4d4d06a5985e014dcdb652ab8195b01","user-agent":"curl/7.79.1","accept":"*/*","authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6InZhZGFsIiwiaXNzIjoickxBczI3d3l5emxWTGhZajA0aFBEU1p5Vm8wREtoNkUiLCJhZG1pbiI6dHJ1ZSwiaWF0IjoxNTE2MjM5MDIyfQ.4-yT__GJURDRb7iL1oe9dGHDRCj58FVkHOX6yw_8gbE","x-consumer-id":"b379cae3-b312-4e44-bfc1-5bc5109216cd","x-consumer-custom-id":"vadal-id","x-consumer-username":"vadal-jwt-k8s","vadal":"is the cloud"}}
```

without the bearer:

```
curl -i localhost/echo 
HTTP/1.1 401 Unauthorized
Date: Wed, 03 Jan 2024 17:19:26 GMT
Content-Type: application/json; charset=utf-8
Connection: keep-alive
Content-Length: 26
X-Kong-Response-Latency: 1
Server: kong/3.5.0
X-Kong-Request-Id: c117d622f1709f356c0a3926fefefb0f

{"message":"Unauthorized"}
```

With an incorrect token:

```
curl -i -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6InZhZGFsIiwiaXNzIjoieHJMQXMyN3d5eXpsVkxoWWowNGhQRFNaeVZvMERLaDZFIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.xjCcyHx0_VSLJCKvTNBawqzdiL8morUyjfSHlVz1oL0" localhost/echo
HTTP/1.1 401 Unauthorized
Date: Wed, 03 Jan 2024 17:20:49 GMT
Content-Type: application/json; charset=utf-8
Connection: keep-alive
Content-Length: 50
X-Kong-Response-Latency: 5
Server: kong/3.5.0
X-Kong-Request-Id: 21813c4468d3e75a567feb51ae7a2d44

{"message":"No credentials found for given 'iss'"}
```

### Replace K8s setting

If you manually delete the kong jwt plugin

then need to use replace:

`kubectl replace -f ../infra/kong/jwt.yml --force`

```
kubectl get secret vadal-jwt-k8s -o jsonpath='{.data}'

{"algorithm":"SFMyNTY=","key":"dmFkYWwtaXNzdWVy","kongCredType":"and0","rsa_public_key":"bm9uZQ==","secret":"YXNlY3JldA=="}
```

This sets up the global JWT plugin which is needed to prevent access without a valid JWT. You need both the global plugin
and the consumer jwt above.

The current jwt.yml does not seem to work by itself.

