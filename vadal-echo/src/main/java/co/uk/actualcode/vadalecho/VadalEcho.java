package co.uk.actualcode.vadalecho;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;

import java.time.LocalTime;
import java.util.List;

@SpringBootApplication
@RestController
@Slf4j
public class VadalEcho {
    public static void main(String[] args) {
        SpringApplication.run(VadalEcho.class, args);
    }

    @GetMapping("/")
    public String echo(@RequestHeader HttpHeaders headers, HttpServletRequest request) {
        headers.put("vadal", List.of(LocalTime.now().toString(), "is the cloud"));
        log.info(headers.toString());
        return headers.toString();
    }

}
