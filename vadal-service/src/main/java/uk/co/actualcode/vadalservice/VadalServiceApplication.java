package uk.co.actualcode.vadalservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class VadalServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(VadalServiceApplication.class, args);
	}

	@GetMapping("/serve")
	public String serve(HttpServletRequest request) {

		return String.format("serve: %s, %s", LocalDateTime.now(), request.getRequestURL());
	}

}
