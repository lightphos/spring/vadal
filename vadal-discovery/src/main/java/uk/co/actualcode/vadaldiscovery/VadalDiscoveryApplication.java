package uk.co.actualcode.vadaldiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class VadalDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(VadalDiscoveryApplication.class, args);
    }

}
