package uk.co.actualcode.vadalgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class VadalGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(VadalGatewayApplication.class, args);
	}

}
