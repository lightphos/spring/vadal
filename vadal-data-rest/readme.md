# Vadal Data Rest

## macos

~/.zshenv

export JAVA_HOME=$(/usr/libexec/java_home)

otherwise get issues with lombok etc

mvn spring-boot:build-image

kubectl create deployment vadal-data-rest — image=vadal-data-rest:0.0.1-SNAPSHOT

brew k9s

k9s - nice

helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install valad-nginx nginx-stable/nginx-ingress

```dtd
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: vadal-ingress
spec:
  ingressClassName: nginx
  rules:
    - host: localhost
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
              service:
                name: vadal-data-rest
                port:
                  number: 8888

```

curl localhost

```dtd
{
  "_links" : {
    "user" : {
      "href" : "http://localhost/u"
    },
    "roles" : {
      "href" : "http://localhost/roles"
    },
    "profile" : {
      "href" : "http://localhost/profile"
    }
  }
}%         
```
