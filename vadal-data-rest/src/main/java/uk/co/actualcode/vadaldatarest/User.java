package uk.co.actualcode.vadaldatarest;

import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.OneToOne;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.RequiredArgsConstructor;


@Data
@NoArgsConstructor
//@RequiredArgsConstructor
@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @OneToOne
    private Role role;

    public User(String name, Role role) {
        this.name = name;
        this.role = role;
    }

}