package uk.co.actualcode.vadaldatarest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootApplication
@RestController
@Slf4j
public class VadalDataRestApplication {


    public static void main(String[] args) {
        SpringApplication.run(VadalDataRestApplication.class, args);
    }

    @Autowired
    UserRepo userRepo;

    @Autowired
    RoleRepo roleRepo;

//    @Bean
//    CommandLineRunner init(UserRepo repo) {
//        return args -> {
//            repo.save(User.builder().name("Fred").build());
//        };
//    }

    @PostConstruct
    public void init() {
        Role boss = new Role("boss");
        Role director = new Role("director");
        roleRepo.saveAll(Arrays.asList(boss, director));
        userRepo.saveAll(Arrays.asList(new User("fred", boss), new User("wilma", director)));
    }

    // note rest repo needs to be outside this class for it to be discovered by spring data
    // this will not work !!!
//    interface UserRepoEmbed extends CrudRepository<User, Long> {
//
//    }

    @GetMapping(value = "/users")
    public Iterable<User> getUsers(HttpServletRequest request) {
        log.info(LocalDateTime.now() + ", " + request.getRequestURL());
        return userRepo.findAll();
    }

}
