package uk.co.actualcode.vadaldatarest;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
public class Role {
    @Id
    @GeneratedValue
    private Long id;

    private String role;

    public Role(String role) {
        this.role = role;
    }
}
