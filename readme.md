# VADAL

### Spring Cloud Services Examples.

- Discovery
- Simple Service
- Gateway
- Admin

### Spring Data Rest
Hateous enabled Spring Data Access

### Docker
Cloud Native Build Pack
https://buildpacks.io/

vadal-data-rest will be deployed to k8s.

mvn spring-boot:build-image

docker run --rm -it -p 7777:7777 vadal-data-rest:0.0.1-SNAPSHOT

### Rabbit MQ on K8s

- vadal-mq-sender
- vadal-mq-receiver
