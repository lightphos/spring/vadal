package uk.co.actualcode.vadalusershc;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.hazelcast.repository.config.EnableHazelcastRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
@EnableHazelcastRepositories
@Slf4j
public class VadalUsersHCApplication {

    public static void main(String[] args) {
        SpringApplication.run(VadalUsersHCApplication.class, args);
    }

    @Autowired
    UserRepo userRepo;

    @PostConstruct
    public void init() {
        List<User> users = Arrays.asList(new User("fred", "boss"), new User("wilma", "director"));
        users.forEach(user -> {
            List<User> result = userRepo.findByName(user.getName());
            if (result.isEmpty()) {
                userRepo.save(user);
            }
        });
    }

    @Bean
    Config config() {
        Config config = new Config();
        JoinConfig join = config.getNetworkConfig().getJoin();
        join.getTcpIpConfig().setEnabled(false);
        join.getMulticastConfig().setEnabled(false);
        join.getKubernetesConfig().setEnabled(true)
                .setProperty("namespace", "vadal")
                .setProperty("service-name", "vhazelcast"); // endpoint name in the service port
        return config;
    }

    @GetMapping(value = "/users")
    public Iterable<User> getUsers(HttpServletRequest request) {
        log.info(LocalDateTime.now() + ", " + request.getRequestURL());
        return userRepo.findAll();
    }

    @PostMapping(value = "/user/{name}/{handle}")
    public User addUser(HttpServletRequest request, @PathVariable("name") String name,
                                  @PathVariable("handle") String handle)  {
        User user = new User(name, handle);
        log.info(LocalDateTime.now() + ", " + request.getRequestURL() + ", " + user);
        return userRepo.save(user);
    }

}
