package uk.co.actualcode.vadalusershc;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
@NoArgsConstructor
@ToString
public class User implements Serializable {

    @Id
    private Long id;
    private String name;
    private String handle;

    public User(String name, String handle) {
        this.name = name;
        this.handle = handle;
    }

}