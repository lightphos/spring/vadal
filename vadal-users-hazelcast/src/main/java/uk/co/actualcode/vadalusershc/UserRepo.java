package uk.co.actualcode.vadalusershc;

import org.springframework.data.hazelcast.repository.HazelcastRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepo extends HazelcastRepository<User, Long> {
    List<User> findByName(@Param("n") String name);
}
