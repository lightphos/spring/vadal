package uk.co.actualcode.vadalmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
@Slf4j
public class VadalMqApplication {

    public static final String VADALQ = "vadalQ";

    public static void main(String[] args) {
        SpringApplication.run(VadalMqApplication.class, args);
    }

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Bean
    public Queue vadalQueue() {
        return new Queue(VADALQ, false);
    }

    @GetMapping("/m/{m}")
    public void send(@PathVariable String m) {
        log.info("Message sent to vadalQ: {}", m);

        rabbitTemplate.convertAndSend(VADALQ, m);
    }
}
