package uk.co.actualcode.vadalusers;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@NoArgsConstructor
@Entity
public class User {
    @Id @GeneratedValue
    private Long id;
    private String name;

    @OneToOne
    private Role role;

    public User(String name, Role role) {
        this.name = name;
        this.role = role;
    }

}