package uk.co.actualcode.vadalusers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@RequiredArgsConstructor
public class Role {
    @Id @GeneratedValue
    private Long id;

    private String role;

    public Role(String role) {
        this.role = role;
    }
}
