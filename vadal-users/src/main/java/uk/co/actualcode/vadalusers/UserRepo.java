package uk.co.actualcode.vadalusers;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "user", path = "u")
public interface UserRepo extends CrudRepository<User, Long> {
    List<User> findByName(@Param("n") String name);
}
