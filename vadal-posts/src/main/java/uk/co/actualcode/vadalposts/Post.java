package uk.co.actualcode.vadalposts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@RequiredArgsConstructor
public class Post {
    @Id @GeneratedValue
    private Long id;

    private Long userId;

    private String user;

    private String topic;
    private String details;

    public Post(Long userId, String topic, String details) {
        this.userId = userId;
        this.topic = topic;
        this.details = details;
    }

}
