package uk.co.actualcode.vadalposts;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
@Slf4j
public class VadalPostApplication {

    public static void main(String[] args) {
        SpringApplication.run(VadalPostApplication.class, args);
    }

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    PostRepo postRepo;

    @Value("${user.service}")
    String userService;

    @Value("${freds.id}")
    Long fredsId;

    @Value("${wilmas.id}")
    Long wilmasId;


    @PostConstruct
    public void init() {
        List<Post> posts = Arrays.asList(
                new Post(fredsId, "freds topic", "rock city works"),
                new Post(wilmasId, "wilmas shop", "stone mall"));
        postRepo.saveAll(posts);

        log.info("User service: {}", userService);
    }


    @GetMapping(value = "/posts")
    public Iterable<Post> getPosts(HttpServletRequest request) {
        log.info(LocalDateTime.now() + ", " + request.getRequestURL());
        Iterable<Post> posts = postRepo.findAll();
        posts.forEach(post -> {
            ResponseEntity<User> userResponse = restTemplate.getForEntity(userService + "/u/" + post.getUserId(), User.class);
            log.info("User returned {}", userResponse.getBody().getName());
            post.setUser(userResponse.getBody().getName());
        });

        return posts;
    }

}
